# Projet: foxes-and-rabbits

Auteurs: Michael Kölling and David J. Barnes
Traduction: Laurent Pierron

Ce projet fait partie du matériel pour le livre

   **Objects First with Java - A Practical Introduction using BlueJ
   Fifth edition
   David J. Barnes and Michael Kölling
   Pearson Education, 2012**

Il est expliqué dans le chapitre 10.

Une simulation classique du modèle proie-prédateur ; renforce les notions d'héritage
et ajoute les classes abstraites et les interfaces.

- - -

## Préparation du projet

1. Créer une bifurcation (fork) du projet **foxes-and-rabbit** dans votre compte Bitbucket.
2. Ajouter votre binôme et `lcpierron` en administrateur du projet.
3. Cloner votre projet sur votre machine locale.

- - -

## Expression du besoin

Le conseil général souhaite connaître l'impact sur la faune de la construction
d'une autoroute, qui partagera en deux le grand domaine forestier dont il
a la charge.

Afin pouvoir prendre une décision éclairée il vous demande de réaliser
une simulation informatique du modèle proie-prédateur (http://www.breves-de-maths.fr/histoire-du-modele-proie-predateur-ou-la-mathematique-des-poissons/),
qui montrera l'évolution de la population de renards et de lièvres dans
la forêt domaniale.

Pour pouvoir visualiser l'évolution de la faune sur le territoire, on montrera
deux vues :

1. une vue qui représentera le terrain avec la répartition des animaux sur le terrain ;
1. un graphique qui montrera l'évolution de la population des renards et des lièvres
au cours du temps.

Voici deux exemples de ces vues :

![simulation_view.png](https://bitbucket.org/repo/L8EA9k/images/231416171-simulation_view.png "Vue du terrain")

![graph_view.png](https://bitbucket.org/repo/L8EA9k/images/853258410-graph_view.png "Graphique d'évolution de la population")

> **Simulations** : Les ordinateurs sont fréquemment utilisés pour simuler des
> systèmes réels. Ces systèmes comprennent la modélisation du trafic routier,
> la météorologie, l'épidémiologie, l'analyse du marché boursier,
> les évolutions environnementales et beaucoup d'autres.
>
> Pour en savoir plus sur la modélisation de l'évolution de population : *INTERSTICES : Des espèces en nombre *(https://interstices.info/jcms/n_49876/des-especes-en-nombre)
>
> Pour réaliser une simulation informatique,
> on essaye de modéliser le comportement d'un sous-ensemble
> du monde réel dans un logiciel.
>
> La simulation peut avoir pour but l'anticipation des évènements,
> la compréhension du monde ou la formation dans un environnement sûr.
>
> En France, l'INRIA (http://www.inria.fr/recherches/domaines-de-recherche/cinq-domaines-de-recherche)
> est un acteur majeur de la modélisation informatique dans le but d'améliorer
> la connaissance de notre univers.

## Analyse

> Intermède musical : http://youtu.be/uz_pJD4zk4k

L'analyse des besoins, les échanges avec le demandeur et notre connaissance
du domaine de la simulation nous permettent de définir le modèle suivant :

![diagramme_classe_initial.png](https://bitbucket.org/repo/L8EA9k/images/3075053685-diagramme_classe_initial.png "Diagramme de classe simplifié")

Outre les classes `Fox` et `Rabbit` issues directement de l'énoncé, qui offrent
une modèlisation de la vie des renards et des lièvres, nous trouvons un certain
nombre de classes utiles pour représenter le modèle d'environnement :

* `Field` : représente le terrain clos en deux dimensions (lignes, colonnes)
sur lequel évolueront les animaux. Le terrain est divisé en cases carrées.
Chaque case ne peut contenir qu'un seul animal à la fois.

* `Location` : représente une position en deux dimensions sur le terrain.

* `Simulator` : est responsable de l'initialisation et de l'exécution
du modèle. Elle fait le lien entre le modèle et la visualisation du modèle.
Le principe du simulateur est simple : il contient le terrain et tous les
animaux et à chaque étape de la simulation, il demande à chaque animal vivant d'agir.
Chaque animal est alors responsable de sa propre action.

Ces cinq classes représentent le modèle de simulation. Les classes suivantes
représentent l'affichage de la simulation :

* `SimulatorView` : réalise l'affichage du terrain avec les animaux, ainsi
que l'affichage des statistiques. On notera que cette classe est fortement
découplée du modèle de simulation, le terrain (classe `Field`) est passé en
paramètre de la fonction d'affichage (`void showStatus(int step, Field field)`).

* `FieldStats` : responsable de calculer le nombre de lièvres et de renard
et de créer une représentation textuelle de ces statistiques. Une fois
encore cette méthode est fortement découplée du reste du programme, le terrain
est passé en paramètre des méthodes de calcul.

* `Counter` : permet de tenir un compteur entier avec un nom. Il est utilisé
par `FieldStats`

Il reste dans le programme une dernière classe un peu spéciale : `Randomizer`.
Cette classe est destinée à instancier un générateur de nombre aléatoire
pour la simulation. Afin de pouvoir effectuer des tests de non-régression,
on souhaite maîtriser la suite de nombre aléatoire créée et avoir le même
générateur pour tout le programme.

Cett classe est spéciale car c'est une classe de type *Singleton*, qui n'a qu'une seule
instance. C'est un moyen de faire des objets globaux dans un programme Java.

> Le terme *Singleton* a été conceptualisé dans le livre *Design patterns - elements of reusable software*, co-écrit par quatre auteurs : Gamma, Helm, Johnson et Vlissides.
> C'est un livre de chevet pour tous les informaticiens faisant de la conception
> orientée objet.
>
> Un autre *patron de conception*, le patron *State* aurait pu être utilisé
> pour réaliser la calculatrice dans un précédent projet.
> Vous découvrirez d'autres patrons de conception en apprenant à réaliser des
> interfaces graphiques.
>
> Pour en savoir plus : http://fr.wikipedia.org/wiki/Patron_de_conception

### Diagramme de séquences

Afin d'affiner l'analyse nous utiliserons des diagrammes de séquence pour montrer les interactions entre
les différents objets dans le programme. Nous allons illustrer cela sur deux cas :

1. le déplacement des lièvres ;
1. la chasse des renards.

![seq_rabbit_run.png](https://bitbucket.org/repo/L8EA9k/images/52729616-seq_rabbit_run.png "")

![seq_fox_hunt.png](https://bitbucket.org/repo/L8EA9k/images/54322223-seq_fox_hunt.png "")

Ces diagrammes peuvent être utilisés pour discuter avec le client sur le comportement attendu de l'application.
On modélise ici la **dynamique** de l'application, alors que le diagramme de classe modélise les aspects **statiques**.

Pour en savoir plus sur les diagrammes de séquences : (http://fr.wikipedia.org/wiki/Diagramme_de_séquence)

- - -

# Semaine 1

- - -

## Ajout de la classe abstraite Animal

Les classes `Fox` et `Rabbit` partagent beaucoup d'éléments en communs, vous
devez fusionner ces élements dans une superclasse `Animal`

### TÂCHE 1 : Création de la superclasse `Animal`

1. Créez la classe `Animal`
1. Faire *hériter* les classes `Fox` et `Rabbit` de cette classe
1. Déplacez les attributs `age`, `alive`, `field` et `location` dans la
superclasse
1. Déplacez les accessseurs et mutateurs corresspondants : `getLocation`,
`setLocation`, `isALive` et `setDead`, passez les méthodes à
`protected` si nécessaire
1. Ajoutez l'accesseur `getField` en `protected` pour pouvoir accéder
à l'attribut `field` depuis les méthodes des classes filles.
1. Compilez et testez
1. Ajoutez votre nouvelle classe au dépôt Git : `git add Animal.java`
1. Committez
1. Synchronisez sur le serveur : `git push --all`


### TÂCHE 2 : Simplification de la classe `Simulator`

1. Dans `Simulator` remplacez les deux listes `rabbits` et `foxes` par une seule
liste `animals`
1. Modifiez les constructeurs de `Simulator` pour tenir compte de la modification des listes.
1. Modifiez la méthode `void reset()`
1. Modifiez la méthode `void populate()`.
1. Modifiez la méthode `void simulateOneStep()`, il peut être nécessaire
d'utiliser l'instruction `instanceof` vue dans le projet précédent.
1. Compilez et testez
1. Committez

Si vous avez réussi, vous noterez, qu'il n'a été aucunement nécessaire de modifier
les classes de visualisation et de statistiques, c'est le signe d'une excellente
conception. En fait vous pouvez ajouter de nouvelles classes d'animaux, sans
modifier autre chose que l'ajout de ces animaux sur le terrain.

La réalisation de la méthode `simulateOneStep` de `Simulator` n'est pas satisfaisante,
car elle doit avoir la forme :

```java
...
for (Iterator<Animal> it = animals.iterator(): it.hasNext(); ) {
  Animal animal = it.next();
  if (animal instanceof Rabbit) {
    Rabbit rabbit = (Rabbit) animal;
    rabbit.run(newAnimals);
  }
  if (animal instanceof Fox) {
    Fox fox = (Fox) animal;
    fox.hunt(newAnimals);
  }
  ...
}
...
```

Comme les deux classes d'animaux n'ont pas la même méthode pour effectuer
une action, et que les méthodes ne sont pas définies au niveau de la superclasse,
il est nécessaire de tester à l'exécution le type de l'objet `Animal` et
de le convertir (*cast*) en un objet `Rabbit` ou `Fox`. Comme on l'a déjà dit
dans le projet précédent l'utilisation de `instanceof` et d'opération de *cast*
est un signe de mauvais conception.

La solution ici consiste à définir dans la classe `Animal`une méthode
de signature : `void act(List<Animal>)`, qui sera instanciée dans chaque sous-classe.
Mais cette méthode ne recevra pas de définition dans la classe `Animal`, ce sera
ce que l'on appelle une méthode **abstraite**, en Java on ajoute le mot-clé
`abstract` devant la déclaration de la méthode et on ne définit pas de corps,
par exemple :

```java
abstract public void act(List<Animal> newAnimals);
```

Quand une méthode est abstraite, toutes les sous-classes doivent implémenter
cette méthode ou la définir comme abstraite.

Une méthode **abstraite** en peut être déclarée que dans une classe également
**abstraite**, qui sera déclarée en Java en plaçant le mot-clé `abstract`
devant le mot-clé `class`, exemple :

```java
public abstract class Animal
{

// Everything omitted

}
```

Les classes qui ne sont pas abstraites sont dites **concrètes**.

Une classe **abstraite** a plusieurs intérêts :

* Aucune instance de la classe ne peut être créée.
* Seules les classes abstraites ont des méthodes abstraites.
* Les classes abstraites avec des méthodes abstraites forcent
les sous-classes *concrètes* à implémenter les méthodes
abstraites.

Maintenant vous pouvez terminer la simplification du programme.

### TÂCHE 3 : Transformation d'`Animal` en classe abstraite

1. Transformez la classe `Animal` en classe abstraite.
1. Ajoutez la méthode abstraite `act` à la classe `Animal`
1. Renommez `act` la méthode `run` de `Rabbit`
1. Renommez `act` la méthode `hunt` de `Fox`
1. Modifiez la méthode `void simulateOneStep()` pour prendre en compte
la méthode `act` de la classe `Animal`.
1. Compilez et testez
1. Committez
1. Ajoutez à la classe `Animal` une méthode abstraite `int getBreedingAge()`,
qui retourne l'âge de reproduction.
1. Implémentez cette méthode dans les classes : `Fox` et `Rabbit`
1. Compilez et testez
1. Committez
1. Maintenant déplacez et réécrivez la méthode `boolean canBreed()` dans la classe
Ànimal` en utilisant `getBreedingAge`
1. Compilez et testez
1. Committez
1. Synchronisez sur le serveur : `git push --all`

- - -

# Semaine 2

- - -

## Ajout de l'interface Actor

Maintenant on souhaite ajouter d'autres élements dans la simulation comme un chasseur
ou l'effet de la météo. Pour faire cela on va ajouter un type de classe assez
particulier appelé une *interface*.
Une *interface* en Java a les caractéristiques suivantes :

* Le mot-clé `interface` est utilisé à la place du mot-clé `class` dans l'entête de
la déclaration.
* Toutes les méthodes sont abstraites. Il n'y a pas de corps de méthodes,
ni de mots-clés `abstract`
* Les interfaces ne contiennent pas de constrcuteurs.
* Toutes les méthodes sont publiques, il n'y a pas de mot-clé `public`.
* Seuls des attributs de classe sont autorisés de type : `public static final`,
les mot-clés peuvent être omis.
* Une classe peut hériter de plusieurs interfaces.

### TÂCHE 4 : Ajout de l'interface `Actor`

1. Créez une interface `Actor`, qui aura les deux méthodes suivantes :

```java
public interface Actor
{
  void act(List<Actor> newActors);

  boolean isActive();
}
```

1. Déclarez la classe `Animal` en sous-classe de `Actor` : `public class Animal implements Actor`
1. Ajoutez la méthode `isActive()` dans `Animal`, vous pouvez soit renommer `isAlive()`,
soit appeler `isAlive()` depuis `isActive()`.
1. Compilez et testez
1. Committez
1. Modifiez `Simulator` pour tenir compte de la classe `Actor`
1. Compilez et testez
1. Ajoutez votre nouvelle classe au dépôt Git : `git add Actor.java`
1. Committez
1. Synchronisez sur le serveur : `git push --all`

## Ajout de la classe Hunter

Maintenant que nous avons une interface `Actor`, nous pouvons ajouter
unz nouvelle classe `Hunter` représentant un chasseur. Le chasseur aura
les propriétés suivantes :

* Les chasseurs n'ont pas d'âge limite, ne se nourrissent pas et en se reproduisent pas.
* A chaque étape de la simulation, un chasseur se déplace n'importe où au hasard
et tire un nombre fixe de coups dans des directions aléatoires.
* Chaque premier animal sous la trajectoire d'une balle est tué.

### TÂCHE 5 : Ajout de la classe `Hunter`


1. Déclarez la classe `Hunter` qui implémente `Actor`.
1. Ajoutez les attributs utiles pour votre classe.
1. Implémentez les méthodes de l'interface.
1. Implémentez les méthodes privées utiles pour votre chasseur.
1. Créez une classe de test pour automatiser le test de votre classe.
1. Compilez et testez
1. Committez
1. Modifiez `Simulator` pour ajouter quelques chasseurs (< 10)
1. Compilez et testez
1. N'oubliez pas d'ajouter votre nouvelle classe au dépôt Git
1. Committez
1. Synchronisez sur le serveur : `git push --all`

## Ajout de la classe TextView

Voici le diagramme de classe UML final dans la branche `graph` :

![diagramme_classe_final.png](https://bitbucket.org/repo/L8EA9k/images/236308589-diagramme_classe_final.png "Diagramme de classe final")

Ce diagramme affiche les deux vues initiales demandées dans l'expression
des besoins. Ces vues implémentent l'*interface* `SimulatorView`.

Il est demandé d'ajouter une nouvelle classe `TextView`qui implémente
`SimulatorView`. `TextView` fournit une vue textuelle de la simulation.
Après chaque pas de simulation? il affiche une ligne de la forme :

```
Temps : 32; Renards : 121; Lièvres : 1023
```

### TÂCHE 6 : Ajout de la classe `TextView`

1. Allez dans la branche `graph` du projet :

```
git fetch --all
git checkout graph
```

1. Ouvrir le projet sous BlueJ
1. Ajoutez la classe `TextView`, qui implémente `SimulatorView`
1. Définissez toutes les méthodes nécessaires de `TextView`
1. Compilez et testez
1. N'oubliez pas d'ajouter votre nouvelle classe au dépôt Git
1. *Committez*
1. Ajoutez une vue `TextView` à la clase `Simulator`, afin que la simulation
sous forme textuelle s'affiche au fil de la simulation.
1. Compilez et testez
1. *Committez*
1. Synchronisez votre travail avec le serveur Bitbucket par la commande : `git push --all`


- - -