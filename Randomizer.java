import java.util.Random;

/**
 * Provide control over the randomization of the simulation.
 * 
 * Design Pattern : Singleton 
 * @see http://www.javaworld.com/article/2073352/core-java/simply-singleton.html
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class Randomizer
{
    // The default seed for control of randomization.
    private static final int SEED = 1111;
    // A shared Random object, if required.
    private static final Random RAND = new Random(SEED);
    // Determine whether a shared random generator is to be provided.
    private static final boolean USE_SHARED = true;

    /**
     * Constructor for objects of class Randomizer
     */
    private Randomizer()
    {
    }

    /**
     * Provide a random generator.
     * @return A random object.
     */
    public static Random getRandom()
    {
        if (USE_SHARED) {
            return RAND;
        }
        else {
            return new Random();
        }
    }
    
    /**
     * Reset the randomization.
     * This will have no effect if randomization is not through
     * a shared Random generator.
     */
    public static void reset()
    {
        if (USE_SHARED) {
            RAND.setSeed(SEED);
        }
    }
}
